<?php

/**
 * @file
 * File formatters for audioBoom audio.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_audioboom_file_formatter_info() {
  $formatters['media_audioboom_audio'] = array(
    'label' => t('Audioboom Audio'),
    'file types' => array('audio'),
    'default settings' => array(
      'width' => '100%',
      'max_height' => 150,
      'protocol' => 'https://',
      'protocol_specify' => FALSE,
    ),
    'view callback' => 'media_audioboom_file_formatter_audio_view',
    'settings callback' => 'media_audioboom_file_formatter_audio_settings',
    'mime types' => array('audio/audioboom'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_audioboom_file_formatter_audio_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'audioboom') {
    $element = array(
      '#theme' => 'media_audioboom_audio',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    foreach (array('protocol', 'protocol_specify', 'attributes') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    return $element;
  }
}