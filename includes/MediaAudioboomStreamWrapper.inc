<?php

/**
 *  @file
 *  Extends the MediaReadOnlyStreamWrapper class to handle Audioboom audio.
 */

class MediaAudioboomStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://audioboom.com/';
  protected $parameters = array('u', 'a');

  static function getMimeType($uri, $mapping = NULL) {
    return 'audio/audioboom';
  }

  function interpolateUrl() {
    $url = "";

    if (isset($this->parameters['u']) && isset($this->parameters['a'])) {
      $url = $this->base_url . check_plain($this->parameters['u']) . '/' . check_plain($this->parameters['a']);
    }

    return $url;
  }

}
