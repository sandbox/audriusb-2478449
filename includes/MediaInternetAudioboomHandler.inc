<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Audioboom audio.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */

class MediaInternetAudioboomHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    $patterns = array(
      '@www.audioboom\.com\/([0-9A-Za-z\@\&\$_-]+)/([0-9A-Za-z\@\&\$_-]+)@i',
      '@audioboom\.com/([0-9A-Za-z\@\&\$_-]+)/([0-9A-Za-z\@\&\$_-]+)@i',
    );

    foreach ($patterns as $pattern) {
      $uri = FALSE;

      preg_match($pattern, $embedCode, $matches);

      if (isset($matches[2]) && !isset($matches[3])) {
        $uri = 'audioboom://u/' . $matches[1] . '/a/' . $matches[2];
      }

      if ($uri) {
        return file_stream_wrapper_uri_normalize($uri);
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    return $file;
  }

}
