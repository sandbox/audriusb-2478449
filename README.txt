CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage

INTRODUCTION
------------

Current Maintainers:

 * Audrius Bukauskas <https://www.drupal.org/u/audriusb>

Media: audioBoom adds audioBoom as a supported media provider.

REQUIREMENTS
------------

Media: audioBoom has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: audioBoom can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: audioBoom integrates the audioBoom audio-sharing service with the
Media module to allow users to add and manage audioBoom audio as they would
any other piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: audioBoom enabled, users can add audioBoom audio by entering its
URL. 
