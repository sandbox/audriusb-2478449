<?php

/**
 * @file
 * Default display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_audioboom_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__default__media_audioboom_audio';
  $file_display->weight = 1;
  $file_display->status = TRUE;
  $file_displays['audio__default__media_audioboom_audio'] = $file_display;

  return $file_displays;
}
