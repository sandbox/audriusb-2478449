<?php

/**
 * @file media_audioboom/themes/media-audioboom-audio.tpl.php
 *
 * Template file for theme('media_audioboom_audio').
 *
 * Variables available:
 *  $uri - The media uri for the audioBoom audio (e.g., soundcloud://u/[user-name]/a/[audio-code]).
 *  $audio_id - The unique identifier of the audioBoom audio (e.g., u/[user-name]/a/[audio-code]).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the audioBoom iframe.
 *  $options - An array containing the Media audioBoom formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: audioBoom file display options.
 *  $height - The height value set in Media: audioBoom file display options.
 *  $title - The Media: audioBoom file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="ab-player" data-boourl="<?php echo $url ?>" data-iframestyle="background-color:transparent; display:block; min-width:300px; max-width:700px;" style="background-color:transparent;"><a href="<?php echo $url_without_embed ?>">listen to audio on audioBoom</a></div><script type="text/javascript">(function() { var po = document.createElement("script"); po.type = "text/javascript"; po.async = true; po.src = "https://d15mj6e6qmt1na.cloudfront.net/cdn/embed.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s); })();</script>