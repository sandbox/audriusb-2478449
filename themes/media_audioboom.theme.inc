<?php

/**
 * @file media_audioboom/themes/media_audioboom.theme.inc
 *
 * Theme and preprocess functions for Media: audrioBoom.
 */

/**
 * Preprocess function for theme('media_audioboom_audio').
 */
function media_audioboom_preprocess_media_audioboom_audio(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();

  $url = "";

  if (isset($parts['u']) && isset($parts['a'])) {
    $url = '/u/' . check_plain($parts['u']) . '/a/' . check_plain($parts['a']);
  }

  $variables['audio_id'] = $url;

  $uri = file_stream_wrapper_uri_normalize('audioboom://' . $url);
  $external_url = file_create_url($uri);

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);


  $query = array();
  $query['url'] = $external_url;

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);

  $variables['alternative_content'] = t('Audio titled @title', array('@title' => $variables['title']));

  // Build the iframe URL with options query string.
  $variables['url'] = url($query['url'] . '/embed/v3');
  $variables['url_without_embed'] = $query['url'];
}
